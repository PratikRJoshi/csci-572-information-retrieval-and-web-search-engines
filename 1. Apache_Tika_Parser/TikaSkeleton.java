
import java.io.*;
import java.util.*;
import java.util.regex.*;

import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.sax.WriteOutContentHandler;

public class TikaSkeleton {

	List<String> keywords;
	PrintWriter logfile;
	int num_keywords, num_files, num_fileswithkeywords;
	Map<String,Integer> keyword_counts;
	Date timestamp;

	/**
	 * constructor
	 * DO NOT MODIFY
	 */
	public TikaSkeleton() {
		keywords = new ArrayList<String>();
		num_keywords=0;
		num_files=0;
		num_fileswithkeywords=0;
		keyword_counts = new HashMap<String,Integer>();
		timestamp = new Date();
		try {
			logfile = new PrintWriter("log.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * destructor
	 * DO NOT MODIFY
	 */
	protected void finalize() throws Throwable {
		try {
			logfile.close();
	    } finally {
	        super.finalize();
	    }
	}

	/**
	 * main() function
	 * instantiate class and execute
	 * DO NOT MODIFY
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		TikaSkeleton instance = new TikaSkeleton();
		instance.run();
	}

	/**
	 * execute the program
	 * DO NOT MODIFY
	 * @throws Exception 
	 */
	private void run() throws Exception {

		// Open input file and read keywords
		try {
			//System.out.println("Current Directory:"+System.getProperty("user.dir"));
			//System.out.println("Path to directory:"+ new File("keyword.txt").getAbsolutePath());
			//System.out.println(new File("keyword.txt").exists());
			//BufferedReader keyword_reader = new BufferedReader(new FileReader(new File("keyword.txt").getAbsolutePath()));
			BufferedReader keyword_reader = new BufferedReader(new FileReader("keywords.txt"));
			String str;
			while ((str = keyword_reader.readLine()) != null) {
				keywords.add(str);
				num_keywords++;
				keyword_counts.put(str, 0);
			}
			keyword_reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Open all pdf files, process each one
		File pdfdir = new File("vault");
		//File pdfdir = new File("./vault");
		File[] pdfs = pdfdir.listFiles(new PDFFilenameFilter());
		try {
			//for(int i=0;i<pdfs.length;){
			for (File pdf:pdfs) {
				num_files++;
				processfile(pdf);
				//processfile(pdfs[i]);
				//i++;
				//System.out.println(i);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Print output file
		try {
			PrintWriter outfile = new PrintWriter("output.txt");
			outfile.print("Keyword(s) used: ");
			if (num_keywords>0) outfile.print(keywords.get(0));
			for (int i=1; i<num_keywords; i++) 
				outfile.print(", "+keywords.get(i));
			outfile.println();
			outfile.println("No of files processed: " + num_files);
			outfile.println("No of files containing keyword(s): " + num_fileswithkeywords);
			outfile.println();
			outfile.println("No of occurrences of each keyword:");
			outfile.println("----------------------------------");
			for (int i=0; i<num_keywords; i++) {
				String keyword = keywords.get(i);
				outfile.println("\t"+keyword+": "+keyword_counts.get(keyword));
			}
			outfile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process a single file
	 * 
	 * Here, you need to:
	 *  - use Tika to extract text contents from the file
	 *  - (optional) check OCR quality before proceeding
	 *  - search the extracted text for the given keywords
	 *  - update num_fileswithkeywords and keyword_counts as needed
	 *  - update log file as needed
	 * 
	 * @param f File to be processed
	 * @throws FileNotFoundException 
	 */
	private void processfile(File f) throws Exception {

		/***** YOUR CODE GOES HERE *****/
		// to update the log file with a search hit, use:
		// 	updatelog(keyword,f.getName());
		
		Parser parser = new PDFParser();
		Metadata metadata = new Metadata();
		BodyContentHandler 	handler = new BodyContentHandler(Integer.MAX_VALUE);		
		FileInputStream fileIn = new FileInputStream(f);
		BufferedInputStream br = new BufferedInputStream(fileIn);
		InputStream inputContent = new BufferedInputStream(br);
		
		try{
			parser.parse(inputContent, handler, metadata, new ParseContext());
			String parsedContent = handler.toString();
			boolean keyFound = false;
			
			for(String keys: keywords){
				int lastIndex = 0;
				int count = 0;
				String search = parsedContent;
				while(lastIndex!=-1){
					Pattern pattern = Pattern.compile("\\b"+keys+"\\b", Pattern.CASE_INSENSITIVE);
					//Pattern pattern = Pattern.compile(keys);
					search = search.substring(lastIndex);
					
					//String[] strArray = search.split(" ");
					//for(int )
					
					Matcher m = pattern.matcher(search);
					
					if(m.find()){
						lastIndex = m.start();
						}
					else
						lastIndex = -1;
					
					if(lastIndex!=-1){
						count++;
						lastIndex+=keys.length();
					}
				}
				if(count>0){
					keyFound = true;
					int totalCount = keyword_counts.get(keys)+count;
					keyword_counts.put(keys, totalCount);
					System.out.println(keys+" "+f.getName()+" "+totalCount+" "+num_fileswithkeywords+" out of"+num_files);
					updatelog(keys, f.getName());
				}
			}
			if(keyFound)
				num_fileswithkeywords++;
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

	/**
	 * Update the log file with search hit
	 * Appends a log entry with the system timestamp, keyword found, and filename of PDF file containing the keyword
	 * DO NOT MODIFY
	 */
	private void updatelog(String keyword, String filename) {
		timestamp.setTime(System.currentTimeMillis());
		logfile.println(timestamp + " -- \"" + keyword + "\" found in file \"" + filename +"\"");
		logfile.flush();
	}

	/**
	 * Filename filter that accepts only *.pdf
	 * DO NOT MODIFY 
	 */
	static class PDFFilenameFilter implements FilenameFilter {
		private Pattern p = Pattern.compile(".*\\.pdf",Pattern.CASE_INSENSITIVE);
		public boolean accept(File dir, String name) {
			Matcher m = p.matcher(name);
			return m.matches();
		}
	}
}