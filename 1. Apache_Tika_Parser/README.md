Apache_Tika_Parser
==================

The Apache Tika Parser designed to parse the given corpus of PDF files

1. The program accepts an input file containing any number of search keyword(s) and/or phrase(s)
2. Iterates through the corpus of data on the local computer.
3. Calls Apache Tika to extract all of the text from each PDF file
4. Scans the extracted text to search for the given search keyword(s)
5. Count and output statistics about
      a. The number of documents that contain the keywords
      b. the total number occurrences of the keywords
      c. list of files containing the keywords
      



The program takes references from the following documentations -

A. Parser interface documentation: http://tika.apache.org/1.4/parser.html

B. PDFParser API: http://tika.apache.org/1.4/api/org/apache/tika/parser/pdf/PDFParser.html

C. BodyContentHandler API: http://tika.apache.org/1.4/api/org/apache/tika/sax/BodyContentHandler.html




The program keeps track of statistics and outputs the following:

1) List of keyword(s) used

2) No of files processed

3) No of files containing keyword(s)

4) No of occurrences (count) of each keyword(s)




It also generates a log file named "log.txt" which contains an entry every time a keyword is found in a file:

1) Filename of PDF file containing keyword

2) Timestamp (time when the file was processed)
