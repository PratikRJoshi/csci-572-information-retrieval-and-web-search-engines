import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.*;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.AbstractUpdateRequest;
import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.update.processor.UpdateRequestProcessor;
//import org.apache.*;
import org.apache.tika.Tika;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Geographic;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import org.geonames.*;



public class TikaHW {

	List<String> keywords;
	PrintWriter logfile;
	int num_keywords, num_files, num_fileswithkeywords;
	Map<String,Integer> keyword_counts;
	Date timestamp;
	List<String> stopwords;
	
	String urlString;
	SolrServer solr;


	

	/**
	 * constructor
	 * DO NOT MODIFY
	 */
	public TikaHW() {
		
		keywords = new ArrayList<String>();
		stopwords = new ArrayList<String>();
		urlString = "http://localhost:8983/solr";
		num_keywords=0;
		num_files=0;
		num_fileswithkeywords=0;
		keyword_counts = new HashMap<String,Integer>();
		timestamp = new Date();
		try {
			logfile = new PrintWriter("log.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * destructor
	 * DO NOT MODIFY
	 */
	protected void finalize() throws Throwable {
		try {
			logfile.close();
	    } finally {
	        super.finalize();
	    }
	}

	/**
	 * main() function
	 * instantiate class and execute
	 * DO NOT MODIFY
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		TikaHW instance = new TikaHW();
		instance.run();
	}

	/**
	 * execute the program
	 * DO NOT MODIFY
	 * @throws Exception 
	 */
	private void run() throws Exception {

		// Open input file and read keywords
		try {
			BufferedReader keyword_reader = new BufferedReader(new FileReader("keywords.txt"));
			String str;
			while ((str = keyword_reader.readLine()) != null) {
				keywords.add(str);
				num_keywords++;
				keyword_counts.put(str, 0);
			}
			keyword_reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		try
		{
		BufferedReader stopword_reader = new BufferedReader(new FileReader("stopwords.txt"));
		String str2;
		while ((str2 = stopword_reader.readLine()) != null) {
			stopwords.add(str2);
			//num_keywords++;
			//keyword_counts.put(str2, 0);
		}
		stopword_reader.close();
	} catch (IOException e) {
		e.printStackTrace();
	}

		 solr = new HttpSolrServer(urlString);
		// Open all pdf files, process each one
		File pdfdir = new File("./vault2");
		File[] pdfs = pdfdir.listFiles(new PDFFilenameFilter());
		for (File pdf:pdfs) {
			num_files++;
			try {
				processfile(pdf);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 solr.commit();	
		}

		// Print output file
		try {
			PrintWriter outfile = new PrintWriter("output.txt");
			outfile.print("Keyword(s) used: ");
			if (num_keywords>0) outfile.print(keywords.get(0));
			for (int i=1; i<num_keywords; i++) outfile.print(", "+keywords.get(i));
			outfile.println();
			outfile.println("No of files processed: " + num_files);
			outfile.println("No of files containing keyword(s): " + num_fileswithkeywords);
			outfile.println();
			outfile.println("No of occurrences of each keyword:");
			outfile.println("----------------------------------");
			for (int i=0; i<num_keywords; i++) {
				String keyword = keywords.get(i);
				outfile.println("\t"+keyword+": "+keyword_counts.get(keyword));
			}
			outfile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process a single file
	 * 
	 * Here, you need to:
	 *  - use Tika to extract text contents from the file
	 *  - (optional) check OCR quality before proceeding
	 *  - search the extracted text for the given keywords
	 *  - update num_fileswithkeywords and keyword_counts as needed
	 *  - update log file as needed
	 * 
	 * @param f File to be processed
	 * @throws Exception 
	 */
	private void processfile(File f) throws Exception {

		/***** YOUR CODE GOES HERE *****/
		// to update the log file with a search hit, use:
		// 	updatelog(keyword,f.getName());
		
		int flag=0;
		InputStream is = null;
		is = new BufferedInputStream(new FileInputStream(f));
		Metadata metadata = new Metadata();
		BodyContentHandler ch = new BodyContentHandler(10*1024*1024);
		Parser parser = new PDFParser();
		ParseContext context = new ParseContext();
		System.out.println("Parsing file:"+f.getName());
		//Tika tika = new Tika();
		try
		{
		parser.parse(is, ch, metadata,context);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		is.close();
		String content = ch.toString();
		/*for(String keywrd : keywords)
		{
			int count=0;
			//int index=0;
			//while(true)
			//{
				
				//int pos = content.indexOf(keywrd1, index);
				//if(pos < 0) 
					//break;
			    String keywrd1 = "\\b"+ keywrd +"\\b";
				Pattern pattern = Pattern.compile(keywrd1,Pattern.CASE_INSENSITIVE);
				Matcher matcher = pattern.matcher(content);
				while(matcher.find())
				{	
				 updatelog(keywrd,f.getName());
				 flag=1;
				 count++;
				} 
				//index = pos+1;
			//}
			int prev_count = keyword_counts.get(keywrd); 
			int total_count = prev_count + count;
			keyword_counts.put(keywrd, total_count);
		}*/
		
		Map<String,Integer> filewords=new HashMap<String,Integer>();
		StringTokenizer st = new StringTokenizer(content," \t\n\r\f.,;:!?''");
		while(st.hasMoreElements())
		{
			int flg=0;
			String word = st.nextElement().toString().toLowerCase();
			if(word.length()>=3)
			{	
			for(String sw : stopwords)
			{
				if(word.equalsIgnoreCase(sw))
				{
					flg=1;
					break;
				}
			}
		     if(flg==0)
		     {	 
			 if(filewords.containsKey(word))
			 {	
			  int curr = filewords.get(word);
			  filewords.put(word, curr+1);
			 }
			 else
			 {
		 		 filewords.put(word, 1);
			 }
		     }
		     else
		    	 flg=0;
			}
		}
		
		/*if(flag == 1)
		{
			flag=0;
			++num_fileswithkeywords;
		}*/
	 
	   Map<String,Integer> sort = sortByComparator(filewords);
	   String first_one="";
	   if(sort.size() != 0)
	      first_one = sort.keySet().iterator().next();
	   Set<String> keySet = sort.keySet();
	   Iterator<String> it = keySet.iterator();
	   
	   String dir = "C:\\Users\\Karthik\\workspace\\TikaHomework\\vault2\\";
	   dir = dir+f.getName();
	   
	   WebService.setUserName("karthikm249"); // add your username here
	   	   // Remember to commit your changes!
	    
	   	   
       int flag2=0;
	   if(!(first_one.equals("")))
	   {
		 //  System.out.println("Highest found keyword: "+first_one+" Frequency="+filewords.get(first_one));  
	     while(it.hasNext())
	     {
	    	 
	    	 String w= it.next();
	    	 if(!(w.contains(".")))
	    	 {		 
	         //System.out.println("( "+w+" , "+filewords.get(w)+" )");
	    	 ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
	  	     searchCriteria.setQ(w);
	  	     ToponymSearchResult searchResult = WebService.search(searchCriteria);
	  	     for (Toponym toponym : searchResult.getToponyms()) {
	  	      if(toponym.getLatitude() != 0)	 
	  	      {
	  	    	  //System.out.println("Word:  "+w+" : "+toponym.getLatitude()+" "+ toponym.getLongitude());
	  	    	  metadata.set(Geographic.LATITUDE, toponym.getLatitude());
	  	    	  metadata.set(Geographic.LONGITUDE, toponym.getLongitude());
	  	    	  SolrInputDocument document = new SolrInputDocument();
	  		      document.addField("id", f.getName());
	  		      document.addField("latitude", toponym.getLatitude());
	  		      document.addField("longitude", toponym.getLongitude());
	  		      document.addField("text", content);
	  		      UpdateResponse response = solr.add(document);
	  		      
	  	    	  /*ContentStreamUpdateRequest up = new ContentStreamUpdateRequest("http://localhost:8983/solr/update/extract");
	  	    	  up.addFile(new File(dir),"application/pdf");
	  	    	  up.setParam("id",f.getName());
	  	    	  up.setParam("latitude",Double.toString(toponym.getLatitude()));
	  	    	  up.setParam("longitude",Double.toString(toponym.getLongitude()));
	  	    	  up.setAction(AbstractUpdateRequest.ACTION.COMMIT, true, true);
	  	    	  solr.request(up);*/
	  	    	  flag2=1;
	  	    	  break;
	  	      }
	  	      
	  	   }
	  	    if(flag2==1)
	  	    {
	  	    	flag2=0;
	  	    	break;
	  	    }
	       } 
	     }
	   }
	   
	   
	   //-------------------------------------------------------------------------------------------------------------
	   
	   /*for(String keywrd : keywords)
	   {
		   //System.out.println(" Keyword: "+keywrd+"  Count: "+sorted.get(keywrd));
		   keyword_counts.put(keywrd, 0);
	   }*/
	   
	}
	
	
	

	/**
	 * Update the log file with search hit
	 * Appends a log entry with the system timestamp, keyword found, and filename of PDF file containing the keyword
	 * DO NOT MODIFY
	 */
	@SuppressWarnings("unused")
	private void updatelog(String keyword, String filename) {
		timestamp.setTime(System.currentTimeMillis());
		logfile.println(timestamp + " -- \"" + keyword + "\" found in file \"" + filename +"\"");
		logfile.flush();
	}

	/**
	 * Filename filter that accepts only *.pdf
	 * DO NOT MODIFY 
	 */
	static class PDFFilenameFilter implements FilenameFilter {
		private Pattern p = Pattern.compile(".*\\.pdf",Pattern.CASE_INSENSITIVE);
		public boolean accept(File dir, String name) {
			Matcher m = p.matcher(name);
			return m.matches();
		}
	}
	
	private static Map<String,Integer> sortByComparator(Map<String, Integer> unsortMap)
    {

        String first="";
		List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());
        Collections.sort(list, new Comparator<Entry<String, Integer>>()
                {
                    public int compare(Entry<String, Integer> o1,
                            Entry<String, Integer> o2)
                    {
                    	return o2.getValue().compareTo(o1.getValue());  
                     }
                });  
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        
       /* for (Entry<String, Integer> entry : sortedMap.entrySet())
        {
            System.out.println("Key : " + entry.getKey() + " Value : "+ entry.getValue());
        }*/
        
        //if(sortedMap.size() != 0) 	
          // first = sortedMap.keySet().iterator().next();
        return sortedMap; 
    } 
 }




