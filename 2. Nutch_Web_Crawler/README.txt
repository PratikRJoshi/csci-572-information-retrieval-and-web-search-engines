README file for Nutch Assignment.

Team Members:
	Abhiraj Pratap Singh Tomar, ID: 5196327215, email: atomar@usc.edu
	Karthik Maddipatla, ID: 6720654296, email: kmaddipa@usc.edu
	Pratik Joshi, ID: 1602215161, email: pratikjo@usc.edu

API versions used: 
	Apache Nutch 1.6
	Hadoop 0.18.0
	
Note: The commands have been configured with the hadoop api folder stored inside the apache nutch folder.
      For example, we used the following directory structure : apache-nutch-1.6/hadoop-0.18.0

Steps to be executed for crawling and pdf extraction:

	Step 1: Crawling the vault mirror site
		command : bin/nutch crawl urls -dir crawl -depth 7 -topN 7000 -threads 50
		
		Notes: Command is supposed to be executed insidehte apache-nutch-1.6 folder
		       The crawled data will be stored in the folder crawl
		
		       We have used the sitemap of the mirror site as the seed url, which has links to all the pdfs.
		       Seed URL : http://baron.pagemewhen.com/~chris/vault/vault.fbi.gov/sitemap.html
		       The pdfs are extracted upon a crawl of depth 5, we kept depth of 7 just to be extra cautious and 
		       not to miss out on any documents.
		       
		       The sitemap has list of around 6000 links, hence a topN value of 7000 is used

	Step 2: Merging the crawl segments
		
		Add the foloowing property to the nutch-site.xml : 
		
			<property>
			 <name>mapred.input.dir</name>
			 <value>*/segments/*/*</value>
			</property>		
			
		Then execute the following two commands:
		
			bin/nutch mergesegs tmp/merged crawl/segments  
			bin/nutch readseg -dump tmp/merged/* tmp/dump
			
		The above commands create a folder tmp and store the merged crawl segments in it
		
	Step 3: Extracting the PDFs
		
		Compile the PDFExtractor.java file (stored in the apache-nutch-1.6 folder)
			
			javac -classpath .:hadoop-0.18.0/hadoop-0.18.0-core.jar:lib/apache-nutch-1.6.jar PDFExtractor.java
			
		Run the compiled code
		
			hadoop-0.18.0/bin/hadoop --config .:hadoop-0.18.0/hadoop-0.18.0-core.jar:lib/apache-nutch-1.6.jar PDFExtractor tmp/merged/* /home/abhiraj/Acads/InfoRet/apache-nutch-1.6/files
			
			Notes: the above program takes 2 arguments from command line:
				1=> The path to the merged crawl segments : tmp/merged/*
				2=> The path to the directory where the extracted PDFs are to be stored
			
			
			
			
			
			
			     
		       
