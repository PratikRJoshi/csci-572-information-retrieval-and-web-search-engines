//JDK imports
import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.InflaterInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
 
//Hadoop imports
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ArrayFile;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.ValueBytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.UTF8;
import org.apache.hadoop.io.VersionMismatchException;
import org.apache.hadoop.io.Writable;
 
//Nutch imports
import org.apache.nutch.metadata.Metadata;
import org.apache.nutch.protocol.Content;
import org.apache.nutch.util.MimeUtil;
import org.apache.nutch.util.NutchConfiguration;

public class PDFExtractor {
	
	public static void main(String argv[]) throws Exception {
		
	    // Path of the directory where the pdfs have to be stored
	    String pdfDir = argv[1];//"/home/abhiraj/Acads/InfoRet/apache-nutch-1.6/files"	 
	
	    String usage = "Content (-local | -dfs &lt;namenode:port&gt;) url segment";
 	    //System.out.println(argv.length);
	    if (argv.length < 2) {
	      System.out.println("usage:" + usage);
	      return;
	    }
	    Configuration conf = NutchConfiguration.create();
	    FileSystem fs = FileSystem.parseArgs(argv, 0, conf);
	    try {
	      String segment = argv[0];
	      System.out.println("Content dir name : "+ Content.DIR_NAME);
	      Path file = new Path(segment, Content.DIR_NAME + "/part-00000/data");
	      SequenceFile.Reader reader = new SequenceFile.Reader(fs, file, conf);
	 
	      Text key = new Text();
	      Content content = new Content();
	      int counter = 1;
	 	
	      while (reader.next(key, content)) {	        
		//System.out.println("Key : "+ key);
		
		//Check if the content type is application/pdf
	        if (content.getContentType().equalsIgnoreCase("application/pdf")) {
	        	//System.out.println("Key : "+ key);
	        	
	        	//Extract portion of url key to get the name for pdf file
	        	String[] parts = key.toString().split("/");
			int len = parts.length;
			
			//There are duplicate urls for the same pdf, so extract content only from the ones which end with at_download/file
			if("at_downloadfile".equals(parts[len-2]+parts[len-1])){
			    //Name for the pdf file
			    String fname = pdfDir +"/"+parts[len-3]+".pdf"; 
			    File f = new File(fname);
			    
			    // Creating a buffered output stream to write the contents to a file
			    FileOutputStream fos = new FileOutputStream(f);
			    BufferedOutputStream bos = new BufferedOutputStream(fos);
			    
			    bos.write(content.getContent());
			    bos.flush();
			    bos.close();
			    counter +=1;
	            }
	        
	        }
	      }
	      reader.close();
	    } finally {
	      fs.close();
	    }
	  }

}
